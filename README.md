This is the set of codes for the **'Image Processing with OpenCV'** tutorials for **Robotix, IIT Kharagpur**.
To run any of them on Ubuntu/Mac OS, install a reasonably recent version of OpenCV, navigate to the folder and use the command:

``` g++ `pkg-config --cflags --libs opencv` <cpp_file_name> -o <executable_name> ```

A sample image is provided in the folders for testing.

**Minimum Requirements:**

- OpenCV (2.4.6 or later)
- g++ ( GNU C++ Compiler )
- [CMake](http://www.cmake.org/cmake-tutorial/) (Version 2.8 or Later)

**Steps to Follow:**

- Clone this repository.
- Go to opencv-cpp-tutorials/
- Run the following commands in the Terminal:
```sh
mkdir build
cd build
cmake ..
```
- To run a particular executable file, type ```make <executable_name>``` inside build. Here the executable name is the name of the corresponding .cpp file. For example, ```make erode_dilate```
- Use ```./<executable_name>``` to run the corresponding binary.