#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "utils.hpp"
#include <iostream>
#include <string>

using namespace std;
using namespace cv;

void GaussianBlurSelf(Mat& src, Mat& dest, int size, double sig=-1.0)
{
	vector < vector<int> > mask;
	int avg;
	GetGaussianMask(mask,avg,size,sig);

	int i,j,mid=(size-1)/2,r=src.rows,c=src.cols;

	for(i=mid;i<r-mid;i++){
		for(j=mid;j<c-mid;j++){
			int sum=0;
			for(int k=-mid;k<=mid;k++){
				for(int l=-mid;l<=mid;l++){
					sum += src.at<uchar>(i+k,j+l)*mask[mid+k][mid+l];
				}
			}
			dest.at<uchar>(i,j) = sum/avg;
		}
	}
}

int main(){

	string fname;
	int size;
	cout<<"Enter name of file:";
	cin>>fname;
	cout<<"Enter ksize: ";
	cin>>size;

	Mat image = imread(fname,0);
	Mat dest = image.clone();
	GaussianBlurSelf(image,dest,size);
	imshow("Blurred",dest);
	waitKey(0);
	dest.release();
	image.release();
	return 0;
}
