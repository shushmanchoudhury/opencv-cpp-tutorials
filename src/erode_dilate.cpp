#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <iostream>
#include <string>

using namespace std;
using namespace cv;

Mat erosion(Mat src,int kernel_size = 3)
{
	unsigned char min;
	Mat result(src.size(),src.type());
	for (int i = kernel_size/2; i < src.rows - kernel_size/2; ++i)
	{
		for (int j = kernel_size/2 ; j < src.cols - kernel_size/2; ++j)
		{
			min = src.at<uchar>(i,j);
			for (int m = i-(kernel_size/2); m <= i+(kernel_size/2) ; ++m)
			{
				for (int n = j-(kernel_size/2); n <= j+(kernel_size/2) ; ++n)
					{
						if(src.at<uchar>(m,n)<min)
							min = src.at<uchar>(m,n);
					}		
			}
			result.at<uchar>(i,j)=min;
		}
	}
	return result;
}

Mat dilation(Mat src,int kernel_size = 3)
{
	unsigned char max;
	Mat result(src.size(),src.type());
	for (int i = kernel_size/2; i < src.rows - kernel_size/2; ++i)
	{
		for (int j = kernel_size/2 ; j < src.cols - kernel_size/2; ++j)
		{
			max = src.at<uchar>(i,j);
			for (int m = i-(kernel_size/2); m <= i+(kernel_size/2) ; ++m)
			{
				for (int n = j-(kernel_size/2); n <= j+(kernel_size/2) ; ++n)
					{
						if(src.at<uchar>(m,n)>max)
							max = src.at<uchar>(m,n);
					}		
			}
			result.at<uchar>(i,j)=max;
		}
	}
	return result;
}

static void help()
{
    cout << "\nThis program demonstrates noise reduction using Morphological Transformations\n"
            "Usage:\n"
            "./erode_dilate <image_name>, Default is ../test_images/erode_dilate.png\n" << endl;
}

int main(int argc, char const *argv[])
{
	const char* file_name = argc >=2 ? argv[1] : "../test_images/erode_dilate.png";
	int size = 3;
	Mat image = imread(file_name,0);
	if(image.empty())
    {
        help();
        cout << "can not open " << file_name << endl;
        return -1;
    }
	namedWindow("Original Image",WINDOW_NORMAL);
	namedWindow("Image after Closing",WINDOW_NORMAL);
	namedWindow("Image after Opening", WINDOW_NORMAL);
	createTrackbar("Kernel_Size","Original Image",&size,20);
	while(1)
	{
		int k_size = size;
		Mat dilated = dilation(image,k_size);
		Mat eroded = erosion(image,k_size);
		Mat closing = erosion(dilated,k_size);
		Mat opening = dilation(eroded,k_size);
		imshow("Original Image",image);
		imshow("Image after Closing",closing);
		imshow("Image after Opening",opening);
		char a = waitKey(27);
		if(a==33)
			break;
	}
	destroyAllWindows();
	return 0;
}