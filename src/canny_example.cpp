#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <iostream>
#include <string>

using namespace cv;
using namespace std;

void UseCanny(Mat img){

	Mat grayscale(img.rows,img.cols,CV_8UC1);	//Initialize a grayscale image matrix
	cvtColor(img,grayscale,CV_BGR2GRAY);		//Convert image to grayscale image using cvtColor function

	int lowthresh = 20, highthresh = 100;		//Set low and high threshold values respectively
	string win_name = "Edge-extracted image";	

	namedWindow(win_name,CV_WINDOW_AUTOSIZE);		//Create a named window using the string defined above
	createTrackbar("Low Threshold",win_name,&lowthresh,150);		//Create trackbar and set min value to lower threshold value
	createTrackbar("High Threshold",win_name,&highthresh,255);		//Create trackbar and set max value	to upper threshold value
	

	while(1){				//Infinite loop	

		Mat result = grayscale.clone();			//Create a copy of grayscale image and store into a new object 'result'
		Canny(result,result,lowthresh,highthresh,3);	//Apply Canny Edge Detection Function using low and high threshold values to result image and overwrite the result image
		imshow(win_name,result);		//Show the image in the window defined by the string win_name
		char ch = waitKey(33);			
		if(ch==27)			//If Esc key is pressed , breakout of the loop
			break;
	}

	grayscale.release(); // Destroy grayscale matrix image
	destroyAllWindows(); // Destroy any open windows
}

int main(){

	string fname;
	cout<<"Enter name of file:";
	cin>>fname;

	Mat image = imread(fname);
	UseCanny(image);
	image.release();
	return 0;
}
