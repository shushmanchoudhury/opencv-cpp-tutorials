#include <cassert>
#include <vector>
#include <cmath>
#include "utils.hpp"

void GetGaussianMask(vector< vector<int> >& mask, int& avg, int size, double sig)
{
	assert(size%2==1);
	int i,j,mid=(size-1)/2;

	mask.resize(size, vector<int>(size));
	avg = 0;

	if(sig<0)
		sig = 0.3*((size-1)*0.5 - 1) + 0.8;

	double factor = exp(mid*mid/(sig*sig));
	for(i=0;i<=mid;i++){
		for(j=0;j<=mid;j++){
			if(i+j==0)
				continue;
			double pwr = (i*i + j*j) /(-2.0*sig*sig);
			mask[mid-i][mid-j] = (int)(exp(pwr)*factor + 0.5);
			mask[mid+i][mid+j] = mask[mid-i][mid+j] = mask[mid+i][mid-j] = mask[mid-i][mid-j];
		}
	}
	mask[mid][mid] = (int)(factor+0.5);
	
	for(i=0;i<size;i++){
		for(j=0;j<size;j++){
			avg+=mask[i][j];
		}
	}
}