#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <iostream>
#include <string>

using namespace cv;
using namespace std;

Mat EdgeDetect(Mat img, int threshold){

	Mat result(img.rows,img.cols,CV_8UC1);   //the result image matrix is initialized
	Mat grayscale(img.rows,img.cols,CV_8UC1);  //the grayscale image matrix is initialized
	cvtColor(img,grayscale,CV_BGR2GRAY);	//Convert image to a grayscale image

	int i,j,k,l,max,min,val,r = img.rows,c = img.cols;

	for(i=1;i<r-1;i++){ 			//loop through rows 1 to r-1 , leaving the rows at the edge
		for(j=1;j<c-1;j++){			//loop through columns 1 to c-1 , leaving the columns at the edge

			max = min = grayscale.at<uchar>(i,j);  //set max and min to the same grayscale value at the current pixel 
			for(k=i-1;k<=i+1;k++){				//loop through the rows in the kernel (3x3) created around pixel(i,j)
				for(l=j-1;l<=j+1;l++){			//loop through the columns in the kernel (3x3) created around pixel(i,j)

					val = grayscale.at<uchar>(k,l);	
					if(val>max)							//Set new maximum value for the given kernel
						max = val;
					else if(val<min)					//Set new minimum value for the given kernel
						min = val;
				}
			}

			if(max-min>threshold)					//Compare difference of maximum and minimum values with the threshold value
				result.at<uchar>(i,j) = 0;			//If difference > threshold , set the pixel value to 0 or black
			else
				result.at<uchar>(i,j) = 255;		//Else if difference < threshold, set the pixel value to 255 or white
		}
	}

	grayscale.release();	//Delete instance grayscale by calling release()
	return result;  		//Return the resultant image with detected edges
}


int main(){

	string file_name;
	int threshold;
	cout<<"Enter name of file:";
	cin>>file_name;
	cout<<"Enter threshold:";
	cin>>threshold;

	Mat image = imread(file_name);
	Mat edge_img = EdgeDetect(image,threshold);
	imshow("Edges Extracted",edge_img);
	waitKey(0);
	image.release();
	edge_img.release();
	return 0;
}