#ifndef UTILS_HPP
#define UTILS_HPP
#include <vector>

using namespace std;
void GetGaussianMask(vector< vector<int> >& mask, int&avg, int size, double sig=-1.0);

#endif